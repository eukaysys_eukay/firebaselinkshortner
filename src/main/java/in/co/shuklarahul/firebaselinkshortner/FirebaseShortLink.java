package in.co.shuklarahul.firebaselinkshortner;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by User on 18-12-2016.
 */

public class FirebaseShortLink {

    private static final String TAG = FirebaseShortLink.class.getSimpleName();
    private static final String FIREBASE_URL = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=";
    private static final String LONG_URL_FIELD = "longDynamicLink";
    private static final String RESPONSE_FILED_SHORT_URL = "shortLink";
    //android
    private static final String DEEP_LINK_FALLBACK_URL = "afl";
    private static final String DEEP_LINK_PACKAGE_NAME = "apn";
    private static final String DEEP_LINK_MIN_VERSION = "amv";
    private static final String DEEP_LINK_APP_DOMAIN = "domain";
    private static final String DEEP_LINK_APP_LINK = "link";
    //ios
    private static final String DEEP_LINK_IOS_BUNDLE_ID = "ibi";
    private static final String DEEP_LINK_IOS_APP_CUSTOM_URL_SCHEME = "ius";
    private static final String DEEP_LINK_IOS_APP_APPSTORE_ID = "isi";
    private static final String DEEP_LINK_IOS_FALLBACK_LINK = "ifl";
    private static final String DEEP_LINK_IOS_IPAD_BUNDLE_ID = "ipbi";
    private static final String DEEP_LINK_IOS_IPAD_FALLBACK_LINK = "ipfl";
    //common
    private static final String DEEP_LINK_SOURCE = "utm_source";
    private static final String DEEP_LINK_IS_AD = "ad";
    private static final String DEEP_LINK_MEDIUM = "utm_medium";
    private static final String DEEP_LINK_CAMPAIGN_ID = "utm_campaign";
    private static final String DEEP_LINK_TERMS = "utm_term";
    private static final String DEEP_LINK_CONTENT = "utm_content";
    private static final String DEEP_LINK_CLIENT_ID = "gclid";
    private static boolean DEBUG = true;
    private ShortLinkCallback mCallback;
    private Uri longlink;
    private String appKey;

    private FirebaseShortLink(ShortLinkCallback callback, String appKey, Uri longlink) {
        mCallback = callback;
        this.longlink = longlink;
        this.appKey = appKey;
    }

    public void BuildShortLink() {
        new GetShortLink(mCallback).execute(longlink.toString(), appKey);
    }

    public interface ShortLinkCallback {
        void onConnected();

        void onSuccess(String shortLink);

        void onFailure();
    }

    public static class ShortLinkBuilder {
        private Map<String, String> deepLinkParams = new HashMap<>(5);
        private Map<String, String> firebaseParam = new HashMap<>(17);
        private String host;
        private String scheme;
        private String appKey;
        private ShortLinkCallback callback;

        public ShortLinkBuilder setAppKey(String key) {
            appKey = key;
            return this;
        }

        public ShortLinkBuilder setDynamicScheme(String scheme) {
            this.scheme = scheme;
            return this;
        }

        public ShortLinkBuilder setDynamicLinkHost(String host) {
            this.host = host;
            return this;
        }

        public ShortLinkBuilder setDynamicAppDomain(String domain) {
            firebaseParam.put(DEEP_LINK_APP_DOMAIN, domain);
            return this;
        }

        public ShortLinkBuilder addParameter(String param, String value) {
            deepLinkParams.put(param, value);
            return this;
        }

        public ShortLinkBuilder setCallback(ShortLinkCallback callback) {
            this.callback = callback;
            return this;
        }

        public ShortLinkBuilder setAndroidFallbackURL(String url) {
            firebaseParam.put(DEEP_LINK_FALLBACK_URL, url);
            return this;
        }

        public ShortLinkBuilder setiOSFallbackURL(String url) {
            firebaseParam.put(DEEP_LINK_IOS_FALLBACK_LINK, url);
            return this;
        }

        public ShortLinkBuilder setiPadFallbackURL(String url) {
            firebaseParam.put(DEEP_LINK_IOS_IPAD_FALLBACK_LINK, url);
            return this;
        }

        public ShortLinkBuilder setPackageName(String androidPackageName) {
            firebaseParam.put(DEEP_LINK_PACKAGE_NAME, androidPackageName);
            return this;
        }

        public ShortLinkBuilder setMinVersion(String version) {
            firebaseParam.put(DEEP_LINK_MIN_VERSION, version);
            return this;
        }

        public ShortLinkBuilder setiOSBundleID(String bundleID) {
            firebaseParam.put(DEEP_LINK_IOS_BUNDLE_ID, bundleID);
            return this;
        }

        public ShortLinkBuilder setiPADBundleID(String bundleID) {
            firebaseParam.put(DEEP_LINK_IOS_IPAD_BUNDLE_ID, bundleID);
            return this;
        }

        public ShortLinkBuilder setiOSCustomURLScheme(String scheme) {
            firebaseParam.put(DEEP_LINK_IOS_APP_CUSTOM_URL_SCHEME, scheme);
            return this;
        }

        public ShortLinkBuilder setiOSAppID(String iOSAppID) {
            firebaseParam.put(DEEP_LINK_IOS_APP_APPSTORE_ID, iOSAppID);
            return this;
        }

        public ShortLinkBuilder setLinkSource(String source) {
            firebaseParam.put(DEEP_LINK_SOURCE, source);
            return this;
        }

        public ShortLinkBuilder setLinkMedium(String medium) {
            firebaseParam.put(DEEP_LINK_MEDIUM, medium);
            return this;
        }

        public ShortLinkBuilder setCampaign(String campaignID) {
            firebaseParam.put(DEEP_LINK_CAMPAIGN_ID, campaignID);
            return this;
        }

        public ShortLinkBuilder setLinkTerms(String linkTerms) {
            firebaseParam.put(DEEP_LINK_TERMS, linkTerms);
            return this;
        }

        public ShortLinkBuilder setLinkContent(String linkContent) {
            firebaseParam.put(DEEP_LINK_CONTENT, linkContent);
            return this;
        }

        public ShortLinkBuilder setLinkClientID(String linkClientID) {
            firebaseParam.put(DEEP_LINK_CLIENT_ID, linkClientID);
            return this;
        }

        public ShortLinkBuilder setAd(boolean isAd) {
            firebaseParam.put(FirebaseShortLink.DEEP_LINK_IS_AD, isAd ? "1" : "0");
            return this;
        }

        public FirebaseShortLink get() {
            Uri.Builder builder = new Uri.Builder().scheme(this.scheme).authority(this.host);
            Set<Map.Entry<String, String>> entries = deepLinkParams.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }
            if (DEBUG) {
                Log.d(TAG, "app deep link : " + builder.build().toString());
            }
            // Build the link with all required parameters
            Uri.Builder longLinkBuilder = new Uri.Builder()
                    .scheme("https")
                    .authority(firebaseParam.remove(DEEP_LINK_APP_DOMAIN) + "app.goo.gl")
                    .path("/")
                    .appendQueryParameter(DEEP_LINK_APP_LINK, builder.build().toString());

            Set<Map.Entry<String, String>> firebaseEntries = firebaseParam.entrySet();
            for (Map.Entry<String, String> entry : firebaseEntries) {
                longLinkBuilder.appendQueryParameter(entry.getKey(), entry.getValue());
            }
            if (DEBUG) {
                Log.d(TAG, "firebase sharable deeplink : " + longLinkBuilder.build().toString());
            }
            return new FirebaseShortLink(this.callback, appKey, longLinkBuilder.build());
        }
    }

    private static class GetShortLink extends AsyncTask<String, Void, String> {
        ShortLinkCallback mCallback;

        GetShortLink(ShortLinkCallback callback) {
            this.mCallback = callback;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject body = new JSONObject();
            String longUrl = strings[0];
            try {
                body.put(LONG_URL_FIELD, longUrl);
            } catch (JSONException e) {
                Log.e(TAG, "Exception occurred :: " + e.getMessage());
                return longUrl;
            }
            try {
                URL connUrl = new URL(FIREBASE_URL + strings[1]);
                HttpsURLConnection httpConnection = (HttpsURLConnection) connUrl.openConnection();

                httpConnection.setConnectTimeout(30000);
                httpConnection.setReadTimeout(20000);
                httpConnection.setRequestMethod("POST");
                httpConnection.setRequestProperty("Content-Type", "application/json");
                httpConnection.setDoOutput(true);
                httpConnection.setDoInput(true);

                if (DEBUG) {
                    Log.d(TAG, " POST request body : " + body.toString());
                }

                OutputStream os = httpConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(body.toString());
                writer.flush();
                writer.close();
                os.close();

                httpConnection.connect();

                int status = httpConnection.getResponseCode();
                InputStream inputStream;
                if (status == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConnection.getInputStream();
                } else {
                    inputStream = httpConnection.getErrorStream();
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder buffer = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                String output = buffer.toString();
                if (DEBUG) {
                    Log.d(TAG, "URLShortnerQuery output : " + output);
                }
                JSONObject outputObject = new JSONObject(output);
                String urlOut = outputObject.getString(RESPONSE_FILED_SHORT_URL);
                if (DEBUG) {
                    Log.d(TAG, "URLShortnerQuery short url : " + urlOut);
                }
                return urlOut;
            } catch (JSONException | IOException e) {
                Log.e(TAG, "Exception occurred :: " + e.getMessage());
            }
            return longUrl;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mCallback.onSuccess(s);
        }
    }
}
